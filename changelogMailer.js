const nodemailer = require("nodemailer")
const moment = require("moment")
const {generateChangelogWithOptions} = require("adanic-auto-changelog")

/**
 * There is something you should know that all variables you define in
 * CI/CD variables automatically added to perocess.env so you can dont pass them
 * from gitlab-ci to here because the will be here automatically
 */
const {
  recipientsMailList, tag, deployerEmail,
  appName, smtpHost, smtpPort,
  smtpUser, smtpPassword, senderEmail,

  //optional: issueUrl should be like  https://issues.apache.org/jira/browse/{id}
  issueUrl,

  //optional:  issue pattern should be like this for jira "[A-Z]+-\d+"
  issuePattern

} = process.env;
console.log("environment variables", process.env)

if (!senderEmail || !recipientsMailList || !deployerEmail
  || !appName || !smtpHost || !smtpPort
  || !smtpUser || !smtpPassword || !senderEmail) {
  throw new Error("These fields are required in process.env ," +
    "recipientsMailList, tag,deployerEmail,\n" +
    "  appName, smtpHost, smtpPort,\n" +
    "  smtpUser, smtpPassword, senderEmail ")
}
const date = moment().format("YYYY/M/D HH:mm:ss")

const template = `
<div class="container">
  <p>Hi every One</p>
  <p>The new version of  ${appName} released</p>
  <p><b>${tag}</b></p>
  <p>Deployed by ${deployerEmail} at ${date}</p>
  <p>Best regards</p>
</div>
`

const transport = nodemailer.createTransport({
  host: smtpHost,
  port: smtpPort,
  secure: true,
  auth: {
    user: smtpUser,
    pass: smtpPassword
  },
  tls: {
    // do not fail on invalid certs
    rejectUnauthorized: false
  }
});


async function mailChangelog() {
  try {
    const fullChangelogPath = __dirname + "/full-changelog.pdf"
    const tagChangelogPath = __dirname + `/${tag}-changelog.pdf`
    await generateChangelogWithOptions({
      issueUrl,
      issuePattern,
      output: fullChangelogPath,
      unreleased: true
    })

    await generateChangelogWithOptions({
      issueUrl,
      issuePattern,
      output: tagChangelogPath,
      unreleased: true,
      tagPattern: tag
    })

    const subject = `${appName} ${tag} released`

    const message = {
      from: senderEmail, // Sender address
      to: recipientsMailList,// List of recipients comma separated like "x@google.com, y@google.com"
      subject,
      html: template,
      attachments: [
        {path: `${tagChangelogPath}`},
        {path: `${fullChangelogPath}`}

      ]
    };

    const result = await transport.sendMail(message)
    console.log("Send email result ", result)
  } catch (e) {
    console.log("mailChangelog error ", e)
    throw e
  }
}

mailChangelog()